# Nomadi
## Il canzoniere
>Testi e accordi delle più famose canzoni dei Nomadi, scritto in $`\LaTeX`$.

Versione `7.2`<br>
Ultimo aggiornamento: 12 Novembre 2023

[Elenco alfabetico delle canzoni](canzoni.md).

## Scarica PDF
Versione più recente: https://gitlab.com/DPDmancul/nomadi/-/jobs/artifacts/master/download?job=compile

Altre versioni: https://gitlab.com/DPDmancul/nomadi/-/jobs/

Anteprima web: https://dpdmancul.gitlab.io/nomadi/

## Genera PDF
Per cambiare l'aspetto del documento modificare il file `Nomadi.tex`.  
Per aggiungere, rimuovere o modificare canzoni agisci nella cartella `canzoni`, prendendo esempio dai file già presenti. Se aggiungi o correggi canzoni considera di fare una Pull Request 😉.

Per generare il PDF usa:
```sh
python3 generate.py
```
Il PDF si troverà nella cartella `build`.

### Requisiti
- $`\LaTeX`$ (`latexmk`)
- Pacchetto `songs` (presente in `texlive-music`)
- Python 3
- calibre (solo per generare epub)

## Pacchetto `songs`
Il canzoniere è stato realizzato utilizzando il pacchetto [songs](https://www.ctan.org/tex-archive/macros/latex/contrib/songs).

## Diritti d'autore
Tutti i testi e le musiche sono coperte dal diritto d'autore. Ogni brano è corredato dall'elenco degli autori.
