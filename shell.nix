{}:
let
  nixpkgs = builtins.fetchTarball {
    url = "https://github.com/nixos/nixpkgs/archive/nixos-22.11.tar.gz";
    sha256 = "1xi53rlslcprybsvrmipm69ypd3g3hr7wkxvzc73ag8296yclyll";
  };
  pkgs = import nixpkgs { };
in
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    (python3.withPackages
      (python-packages: with python-packages; [
        dominate
      ]))
    (texlive.combine {
      inherit (texlive)
        scheme-medium
        alegreya
        fontaxes
        collection-music
        stackengine
        xassoccnt
        tikzpagenodes
        ifoddpage
        adjustbox
        collectbox
        ;
    })
    calibre
  ];
}
