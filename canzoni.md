# Nomadi
## Il canzoniere
[← indietro](README.md)
## Indice alfabetico canzoni


### #

1. **20 di Aprile** (20 de abril)  
		_E. Perez Lopez – I. Castro Barba – C. Soto Cobos – C. Cuenca Lazaro – A. Garcia Gonzales – G. Martin Oscar – J. Hernandez Cifuentes – I. Martin – G. Jeves Gonzales – G. Carletti_
2. **32° parallelo**   
		_M. Petrucci – G. Carletti_
3. **60 - 70 - 80**   
		_A. Daolio – R. Rossi – G. Carletti – C. Dennis_

### A

4. **Abbi cura di te**   
		_L. Albertelli – G. Carletti_
5. **Abbracciami ancora una volta**   
		_A. Maccagno – C. Falzone – G. Nicolosi – G. Carletti – M. Corti – M. Vecchi – S. Reggioli_
6. **Addormentato ma non troppo**   
		_C. Turato – D. Campani – G. Carletti – C. Falzone_
7. **Ad est, ad est**   
		_D. Taurian – G. Carletti – O. Veroli_
8. **Agguanta il leone**   
		_A. Daolio – C. Dennis – Gionata – R. Rossi – G. Carletti_
9. **Aiutala**   
		_A. Daolio – G. Carletti – C. Dennis – R. Rossi_
10. **Ala bianca** (Sixty years on)  
		_L. Albertelli – B. Taupin – E. John_
11. **Amici miei**   
		_G. Cortesi_
12. **Amore che prendi amore che dai**   
		_G. Carletti – M. Vecchi – M. Chiarelli_
13. **Amore grande mio**   
		_O. Veroli_
14. **Ancora ci sei**   
		_C. Turato – G. Carletti_
15. **Ancora non so**   
		_L. Cerquetti – A. Mei – D. Campani – G. Carletti_
16. **Animante**   
		_N. Marino – C. Turato – G. Carletti_
17. **Anni di frontiera**   
		_A. Gentili – D. Sacco – G. Carletti – M. Petrucci_
18. **Apparenze**   
		_G. Carletti – C. Turato_
19. **Asia**   
		_F. Guccini_
20. **Auschwitz** (La canzone del bambino nel vento)  
		_F. Guccini_
21. **Autogrill**   
		_F. Guccini_

### B

22. **Babilonia**   
		_M. Bettelli_
23. **Balla Piero** (La ballata)  
		_R. Rossi – Gionata_
24. **Baradukà**   
		_G. Tagliazucchi_
25. **Beautiful day**   
		_C. Contini – G. Carletti_
26. **Bianchi e neri**   
		_G. Cortesi_
27. **Buonanotte ai sognatori**   
		_P. Turtoro – G. Carletti_

### C

28. **Calimocho**   
		_F. Ferrandi – G. Carletti – M. Petrucci – M. Rettani – Y. Ciloni_
29. **Cammina, cammina**   
		_A. Daolio – G. Carletti – O. Veroli_
30. **Canto alla Luna**   
		_E. Toffoli - G. Carletti_
31. **Canto d'amore**   
		_Aiax – T. Verona_
32. **Canzone d'amore**   
		_A. Daolio – G. Carletti – C. Dennis_
33. **Canzone della bambina portoghese**   
		_F. Guccini_
34. **Canzone per i desaparecidos**   
		_G. Carletti – O. Veroli_
35. **Canzone per un'amica**   
		_F. Guccini_
36. **Casa mia**   
		_A. Daolio – C. Dennis – G. Carletti_
37. **C'eri anche tu**   
		_C. Falzone – G. Carletti – E. Pietrelli – L. Garrilli – Y. Cilloni_
38. **Certo che puoi**   
		_G. Carletti – R. Robin – M. vecchi_
39. **C'è un re**   
		_A. Daolio – G. Carletti – O. Veroli_
40. **Chiamami**   
		_G. Carletti – F. Ferrandi – M. Petrucci – A. Roveroni – C. Turato_
41. **Chi mi aiuterà** (You keep me hangin' on)  
		_B. Holland – L. H. Herbert – E. Holland – R. Gianco_
42. **Chi sei**   
		_G. Carletti_
43. **Ci vuole distanza**   
		_A. Mei – L. Cerquetti – G. Carletti – C. Falzone_
44. **Ci vuole un senso**   
		_D. Sacco – E. Carletti – G. Carletti – G. Moretti – M. Arveda_
45. **Colori**   
		_G. Carletti – C. Contini_
46. **Colpa della luna**   
		_A. Daolio – G. Carletti – O. Veroli_
47. **Come mai**   
		_G. Carletti – R. Rossi_
48. **Come potete giudicar**   
		_T. Verona – S. Bono_
49. **Come un fiume**   
		_G. Carletti – D. Campani –  P. Turtolo_
50. **Come va la vita**   
		_G. Carletti – L. Cerquetti – C. Falzone – E. Tiberi – M. Vecchi_
51. **Confesso**   
		_A. Mei – L. Cerquetti – G. Carletti – S. Reggioli_
52. **Con gli occhi di chi**   
		_E. Pietrelli – G. Carletti – M. Vecchi – S. Dolci_
53. **Con me o contro di me**   
		_C. Cattini – D. Campani – D. Sacco – G. Carletti – G. Salvatori_
54. **Contro**   
		_G. Carletti – O. Veroli_
55. **Corpo estraneo**   
		_A. Mei – L. Cerquetti – G. Carletti – C. Falzone_
56. **Cosa cerchi da te**   
		_G. Carletti – M. Vecchi – L. Cerquetti – L. Trentacarlini_
57. **Così sia**   
		_G. Carletti – C. Cattini – M. Vecchi_
58. **Costa dell'Est**   
		_R. Mazzei_
59. **Crescerai**   
		_A. Daolio – G. Carletti – V. Tempera – C. A. Contini_

### D

60. **Dalla parte del cuore**   
		_C. Falzone – D. Campani – F. Ferrandi – G. Carletti – M. Petrucci_
61. **Dammi un bacio** (Antonio Ligabue)  
		_A. Daolio – G. Carletti – O. Veroli_
62. **Dam un bes** (Dammi un bacio)  
		_A. Daolio – G. Carletti – O. Veroli_
63. **Decadanza**   
		_F. Ferrandi – G. Carletti – M. Petrucci – M. Rettani – M. Vecchi_
64. **Dio è morto** (se Dio muore, è per tre giorni poi risorge)  
		_F. Guccini_
65. **Domani**   
		_Mogol – M. Lavezzi_
66. **Donna**   
		_G. Carletti – O. Veroli – E. Toffoli_
67. **Donna la prima donna**   
		_Mogol – D. DiMucci – E. Maresca_
68. **Dove sei**   
		_G. Carletti – R. Robin – D. Pignozzo_
69. **Dove si va**   
		_C. Cattini – G. Carletti – D. Sacco – M. Vecchi_
70. **Due re senza corona**   
		_A.Mei – G. Carletti – L. Cerquetti – M. Vecchi_

### E

71. **E di notte**   
		_G. Carletti – D. Sacco – L. Cerquetti – A. Mei_
72. **Ed io**   
		_M. Dallari – F. Ceccarelli_
73. **Edith**   
		_Cornella – M. Fumai_
74. **È giorno ancora**   
		_F. Guccini_
75. **E il treno va**   
		_J. Plante – D. pace – G. C. Testoni_
76. **Esci fuori** (Al mio paese)  
		_G. Carletti – S. Reggioli – C. Turato_
77. **Essere o non essere**   
		_A. Mei – L. Cerquetti – G. Carletti – D. Campani_
78. **Estate**   
		_M. Dallari – F. Ceccarelli_
79. **Eterno**   
		_C. Contini – G. Carletti_
80. **E vorrei che fosse**   
		_C. Contini – B. Tavernese_

### F

81. **Fatti miei**   
		_B. Tavernese – A. Salerno_
82. **Fidati di me**   
		_D. Campani – G. Carletti – M. Vecchi – Y. Cilloni_
83. **Figli dell'oblio**   
		_D. Campani – E. Carletti – G. Carletti – C. Epifano – C. Falzone – G. Marino – M. Vecchi_
84. **Film**   
		_G. Carletti_
85. **Frasi nel fuoco**   
		_E. Toffoli – G. Carletti – M. Vecchi – Y. Cilloni_
86. **Fuori**   
		_B. Rosset – M. Vecchi – L. Zannoni – G. Carletti_
87. **Fuori la paura**   
		_F. Ferrandi – G. Carletti – M. Petrucci – M. Vecchi_

### G

88. **Giorni tristi**   
		_T. Verona – Mozzarini_
89. **Giorno d'estate**   
		_F. Guccini_
90. **Gira**   
		_Gionata – R. Rossi_
91. **Gli aironi neri**   
		_A. Daolio – G. Carletti – O. Veroli_
92. **Gordon**   
		_R. Rossi – G. Carletti_
93. **Guai se...** (White shadow)  
		_D. Taurian – G. Carletti – O. Veroli_
94. **Guantanamera**   
		_J. Fernandez_

### H

95. **Hasta siempre Comandante**   
		_C. Puebla_
96. **Hey man**   
		_Z. Fornaciari – G. Paoli_
97. **Hey Nomadi**   
		_A. Daolio – C. Dennis – B. Carletti_
98. **Ho difeso il mio amore** (Nights In White Satin)  
		_D. Pace – J. Hayward_

### I

99. **Icaro** (Winter of my life)  
		_B. Waddell – S. Hammond – L. Albertelli_
100. **Ieri sera sognavo di te**   
		_P. Limiti – E. Suligoj – M. Seimandi_
101. **I gatti randagi**   
		_Gionata – R. Rossi_
102. **Il ballo della sedia**   
		_D. Sacco – G. Carletti – M. Vecchi_
103. **Il circo è acceso**   
		_G. Carletti – M. Vecchi – M. Chiarelli_
104. **Il confine**   
		_A. Salerno – Richhi_
105. **Il destino**   
		_C. Contini – G. Carletti_
106. **Il disgelo**   
		_F. Guccini_
107. **Il falò**   
		_R. Rossi – G. Carletti – C. Dennis – A. Daolio_
108. **Il fiore nero**   
		_Gionata – R. Rossi_
109. **Il fiume**   
		_G. Carletti – O. Veroli – A. Daolio_
110. **Il gigante** (Dear Elaine)  
		_L. Albertelli – R. Wood_
111. **Il giorno di dolore che uno ha**   
		_L. Ligabue_
112. **Il libero**   
		_E. Toffoli – G. Carletti – O. Veroli_
113. **Il mattino dopo**   
		_G. Carletti – O. Veroli_
114. **Il mondo piange**   
		_A. Fornaciari – I. Fornaciari – D. Dattoli_
115. **Il mongolo**   
		_G. Carletti – R. Rossi – O. Veroli_
116. **Il muro**   
		_D. Taurian – G. Carletti – O. Veroli_
117. **Il musicista**   
		_D. Longo – G. Carletti – O. Veroli_
118. **Il nome che non hai**   
		_G. Carletti – D. Sacco – E. Carletti_
119. **Il nome di lei** (Gotta see Jane)  
		_R. D. Taylor – E. Holland – R. Miller – P. Dossena_
120. **Il nulla**   
		_G. Carletti – D. Campani – D.Sacco_
121. **Il paese**   
		_G. Cortesi – G. Carletti_
122. **Il paese delle favole**   
		_G. Carletti – R. Rossi_
123. **Il pilota di Hiroshima**   
		_A. Daolio – C. Dennis – G. Carletti – R. Rossi_
124. **Il profumo del mare**   
		_G.Tropeano – A.Valente – G.Carletti_
125. **Il re è nudo**   
		_G. Carletti – L. Cerquetti – A. Mei – C. Falzone_
126. **Il saggio**   
		_G. Carletti_
127. **Il sapore della vita**   
		__
128. **Il segno del FuoriClasse**   
		_F. Ferrandi – G. Carletti – M. Petrucci – M. Vecchi – Y. Cilloni_
129. **Il seme**   
		_A. Daolio – G. Carletti – R. Rossi – C. Dennis_
130. **Il serpente piumato**   
		_G. Cortesi_
131. **Il tavolino**   
		_G. Carletti – R. Rossi – O. Veroli_
132. **Il treno della notte**   
		_A. Daolio – G. Carletti – C. Dennis_
133. **Il vecchio e il bambino**   
		_F. Guccini_
134. **Il vento del nord**   
		_G. Carletti – O. Veroli – C. Falzone – L. Cerquetti_
135. **Il vento tra le mani**   
		_G. Gismondi – S. Reggioli – D. Campani – G. Carletti_
136. **Il viaggiatore**   
		_G. Carletti – O. Veroli – C. Falzone – L. Cerquetti_
137. **I miei anni** (Il bilancio)  
		_Gionata – R. Rossi_
138. **Immagina un momento**   
		_F. Varchetta – R. Rossi – B. Carletti_
139. **Immagini**   
		_G. Carletti – C. Contini_
140. **In favelas**   
		_G. Carletti – O. Veroli – M. Petrucci_
141. **In piedi**   
		_G. Carletti – D. Sacco_
142. **In questo silenzio**   
		_A. Mei – G. Carletti – L. Cerquetti – M. Vecchi_
143. **Insieme io e lei** (Days)  
		_R. Davies – T. Verona_
144. **Io canto perché** (Aspettando la giustizia)  
		_F. Varchetta – R. Rossi – B. Carletti_
145. **Io ci credo ancora**   
		_F. Montesi – G. Carletti – S. Cenci – S. Reggioli_
146. **Io come te**   
		_E. Carletti – G. Carletti – C. Cattini – M. Vecchi_
147. **Io non sono io**   
		_G. Carletti – C. Contini – D. Gilocchi_
148. **Io sarò**   
		_F. Ferrandi – G. Carletti – M. Petrucci – M. Rettani – M. Vecchi_
149. **Io vagabondo**   
		_A. Salerno – D. Dattoli_
150. **Io voglio vivere**   
		_G. Carletti – M. Vecchi – A. Mei – L. Cerquetti_
151. **I ragazzi dell'olivo**   
		_A. Daolio – G. Carletti – O. Veroli_
152. **Isola ideale**   
		_G. Carletti – C. Contini_
153. **I tre miti**   
		_M. Dallari – F. Ceccarelli – O. Veroli_

### J

154. **Jenny**   
		_F. Betty_
155. **Joe Mitraglia**   
		_M. Bettelli_
156. **Johnny**   
		_G. Carletti – O. Veroli – M. Petrucci_

### L

157. **La bomba**   
		_R. Rossi – G. Carletti_
158. **La città**   
		_Gionata – R. Rossi_
159. **La coerenza**   
		_G. Kuzminac – S. Contin – O. Veroli – G. Carletti_
160. **La collina**   
		_F. Guccini_
161. **La deriva**   
		_Urzino – G. Carletti_
162. **La devianza**   
		_R. Rossi – Gionata_
163. **La dimensione**   
		_A. Mei – G. Carletti – L. Cerquetti – M. Vecchi_
164. **Là dove stanno gli dei**   
		_G. Carletti – O. Veroli – D. Taurian_
165. **Ladro di sogni**   
		_G. Carletti – O. Veroli – D. Taurian_
166. **La leva calcistica della classe '68**   
		_F. De Gregori_
167. **La libertà di volare**   
		_G. Carletti – M. Chiarelli_
168. **La mia canzone per gli amici**   
		_M. Bettelli_
169. **La mia città**   
		_F. Ceccarelli – M. Dallari_
170. **La mia libertà** (Girl Don't Tell Me)  
		_B. Wilson – T. Verona_
171. **La mia terra**   
		_C. Cattini – S. Oliva – M. Vecchi – G. Carletti_
172. **La morale**   
		_G. Carletti – R. Rossi_
173. **L'angelo caduto**   
		_G. Carletti – L. Fontana – C. Falzone_
174. **La rosa del deserto**   
		_G. Carletti – l. Cerquetti – A. Mei_
175. **L'arte degli amanti**   
		_G. Carletti – A. Mei – L. Cerquetti – S. Reggioli_
176. **Lascia il segno**   
		_G. Carletti – C. Epifano – G. Marino – M. Vecchi – C. Turato_
177. **La settima onda**   
		_G. Carletti – O. Veroli – D. Taurian_
178. **La storia**   
		_G. Carletti - C. Contini_
179. **L'atomica cinese**   
		_F. Guccini_
180. **L'auto corre lontano, ma io corro da te** (Wichita lineman)  
		_Mogol – J. Webb_
181. **L'aviatore**   
		_C. Cattini – G. Carletti – M. Vecchi_
182. **La vita che seduce**   
		_G. Carletti – L. Cerquetti – A. Mei_
183. **La vita è mia**   
		_C. Falzone – G. Carletti – G. Martellieri – M. Arveda – S. Reggioli – S. Tagliati_
184. **La voce dell'amore**   
		_S. Flores – G. Carletti – M. Vecchi_
185. **La voglia di posare** (La crisi)  
		_R. Rossi – G. Carletti_
186. **Le leggende di un popolo**   
		_G. Tropeano – A. Valente – G. Carletti_
187. **Le poesie di Enrico**   
		_G. Carletti – O. Veroli – D. Taurian_
188. **L'eredità**   
		_M. Petrucci – G. Carletti – O. Veroli_
189. **Le strade**   
		_G. Carletti – O. Veroli – M. Petrucci_
190. **L'Europa**   
		_P. Turtoro – G. Carletti – M. Rettani – M. Vecchi – D. Campani_
191. **L'isola che non c'è**   
		_E. Bennato_
192. **L'isola non trovata**   
		_F. Guccini_
193. **Lontano**   
		_Urzino – Gionata_
194. **L'ordine dall'alto**   
		_C. Cattini – M. Vecchi – G. Carletti_
195. **L'orizzonte di Damasco**   
		_G. Carletti_
196. **Lo specchio ti riflette** (El espejo te delata)  
		_C. Campani – G. Carletti – P. Turro_
197. **Luisa**   
		_M. Bettelli_
198. **L'ultima salita**   
		_M. Dapit – G. Carletti – M. Liverani_
199. **Lungo le vie del vento**   
		_G. Carletti – O. Veroli – D. Taurian_
200. **L'uomo di Monaco**   
		_A. Daolio – R. Rossi – G. Carletti - C. Dennis_

### M

201. **Ma che film la vita**   
		_A. Daolio – G. Carletti – O. Veroli_
202. **Mai come lei nessuna** (Run to the sun)  
		_P. Tubbs_
203. **Mamma Giustizia**   
		_R. Ortolani – J. Fiastri_
204. **Mamma musica**   
		_G. Carletti – O. Veroli – D. Taurian_
205. **Ma noi no**   
		_A. Daolio – G. Carletti – O. Veroli_
206. **Ma piano** (Per non svegliarmi)  
		_G. Meccia_
207. **Marinaio di vent'anni**   
		_D. Taurian – G. Carletti – O. Veroli_
208. **Marta**   
		_J. Carioli – G. Carletti – P. Bettazzi_
209. **Mediterraneo**   
		_M. Petrucci – G. Carletti_
210. **Mercanti e servi**   
		_O. Veroli – A. Daolio – G. Carletti_
211. **Michelina**   
		_P. Milanesi_
212. **Milleanni**   
		_E. Carletti – M. Vecchi_
213. **Mille e una sera**   
		_G. Carletti – L. Albertelli – D. Gilocchi_
214. **Monna Cristina**   
		_T. Verona – F. Fabbri_
215. **Monna Lisa**   
		_I. Graziani_

### N

216. **Naracauli**   
		_M. Bettelli_
217. **Né gioia né dolore**   
		_P. Mancini – G. Carletti – O. Veroli_
218. **Nei miei sogni**   
		_G. Carletti – O. Veroli – G. Vecchiato_
219. **Nella sera**   
		_A. Daolio – G. Carletti – C. Dennis_
220. **Noi**   
		_F. Guccini_
221. **Noi non ci saremo**   
		_F. Guccini_
222. **Nomadi**   
		_F. Guccini_
223. **Non avrai**   
		_C. Cattini – E. Carletti – M. Vecchi_
224. **Non c'è tempo da perdere**   
		_G. Carletti – C.Falzone – C. Turato_
225. **Non credevi**   
		_M. Bettelli_
226. **Non dimenticarti di me**   
		_Mogol – M. Lavezzi_
227. **Non è un sogno**   
		_M. Vattai – M. Vecchi – G. Carletti_
228. **Non so io ma tu**   
		_C. Falzone – D. Sacco – G. Carletti – I. Munari – M. Arveda – M. Greghi_
229. **No vale la pena**   
		_Duo Trinitario – Coco di Cuba_
230. **Nulla di nuovo**   
		_G. Carletti – C. Cattini – C. Falzone – M. Vecchi_
231. **Nuvole basse**   
		_F. Ceccarelli - Dallari_

### O

232. **Occhi aperti**   
		_L. Cerquetti – A. Mei – C. Falzone – G. Carletti_
233. **Oceano**   
		_G. Carletti – C. Contini_
234. **Ogni cosa che vivrai**   
		_G. Carletti – D. Campani – M. Vecchi – E. Pietrelli – S. Grimaldi – Y. Cilloni_
235. **Ophelia**   
		_F. Guccini_
236. **Oriente**   
		_A. Mei – G. Carletti – C. Falzone – L. Cerquetti_
237. **Origini**   
		_G. Cortesi_

### P

238. **Passion flower** (Tout l'amour cha cha – Für Elise)  
		_L. van Beethoven – B. Botkin – G. Garfield – P. Murtagh_
239. **Per fare un uomo**   
		_F. Guccini – V. Tempera_
240. **Per fare un uomo**   
		_F. Guccini_
241. **Per quando è tardi**   
		_F. Guccini_
242. **Per quando noi non ci saremo**   
		_F. Guccini – G. Carletti_
243. **Piccola città**   
		_F. Guccini_
244. **Medley: Piero e Cinzia – Redemption song**   
		_A. Venditti – B. Marley_
245. **Pietro**   
		_D. Taurian – G. Carletti – O. Veroli_
246. **Prenditi un po' di te**   
		_G. Carletti – M. Vattai – M. Vecchi_
247. **Prima del temporale**   
		_E. Ruggeri – L. Schiavone_
248. **Priavera di Praga**   
		_F. Guccini_
249. **Può succedere**   
		_G. Carletti – l. Cerquetti – E. Munda_

### Q

250. **Quando ci sarai**   
		_M. Dapit – G. Carletti – O. Veroli_
251. **Quanti anni ho?**   
		_A. Salerno – D. Dattoli_
252. **Quasi quasi**   
		_B. Tavernese – L. Albertelli_
253. **Quattro lire e noi** (My mind's eye)  
		_R. Lane  – S. Marriott – V. Pallavicini_
254. **Qui**   
		_G. Carletti – M. Petrucci – S. Reggioli_

### R

255. **Racconta tutto a me** (You don't love me)  
		_D. Raye – T. Verona_
256. **Rebecca** (Un gioco di società)  
		_M. Bettelli_
257. **Ricomincia così**   
		_D. Pigozzo – N. Pietrogrande – G. Carletti – S. Reggioli_
258. **Ricordarti**   
		_G. Carletti – O. Veroli – D. Taurian_
259. **Ricordati di Chico**   
		_G. Cortesi_
260. **Ricordi** (Se restiamo ancora qui)  
		_G. Carletti_
261. **Ritornerei**   
		_C. Contini – B. Tavernese_
262. **Riverisco**   
		_G. Carletti – R. Rossi_
263. **Rosso**   
		_P. Farri_
264. **Rotolando va**   
		_R. Rossi – A. Daolio – G. Carletti – C. Dennis_
265. **Rubano le fate**   
		_G. Carletti – P. Turtoro – M. Vecchi_

### S

266. **Salutami le stelle**   
		_A. Daolio – G. Carletti – O. Veroli – P. Milanesi_
267. **Salvador** (15 anni dopo)  
		_G. Cortesi_
268. **Salve sono la Giustizia**   
		_G. Carletti – M. Chiarelli – D. Sacco – C. Falzone_
269. **Sangue al cuore**   
		_L. Cerquetti – D. Sacco – G. Carletti – A. Mei_
270. **Santina**   
		_G. Cortesi_
271. **Sarà come il tempo vuole**   
		_G. Carletti – O. Veroli – A. Daolio_
272. **Sassofrasso**   
		_G. Carletti – O. Veroli – R. Rossi_
273. **Segnali caotici**   
		_R. Rossi – G. Carletti – A. Daolio – C. Dennis_
274. **Sempre di corsa**   
		_P. Vaccari_
275. **Se non ho te**   
		_D. Sacco – G. Carletti_
276. **Senza discutere**   
		_A. Salerno – U. Napolitano – C. M. Ferilli_
277. **Senza nome**   
		_D. Sacco – G. Carletti – M. Vecchi_
278. **Senza patria**   
		_G. Carletti – O. Veroli – A. Daolio_
279. **Senza pensieri nel cuore**   
		__
280. **Sera bolognese**   
		_F. Ceccarelli – M. Dallari_
281. **Se restiamo ancora qui** (Ricordi)  
		_G. Carletti_
282. **So che mi perdonerai**   
		_Mogol – B. Lauzi – D. Dattoli – Merendero_
283. **Soffio celeste**   
		_E. Mattei – G. Carletti – E. Pietrelli_
284. **Soldato**   
		_A. Mei – G. Carletti – L. Cerquetti_
285. **Solo esseri umani**   
		_D. Campani – D. Pignozzo – G. Carletti – M. Vecchi – N. Pietrogrande – Y. Cilloni_
286. **Soltanto un gioco**   
		_D. Taurian – G. Carletti – O. Veroli_
287. **Sorprese**   
		_G. Carletti – R. Rossi_
288. **Sospesi tra terra e cielo**   
		_G. Carletti – M. Chiarelli – E. Carletti_
289. **Spegni quella luce**   
		_O. Veroli – Pontiack – T. Verona_
290. **Stagioni** (Seasons)  
		_L. Albertelli – B. Taupin – E. John_
291. **Statale 17**   
		_F. Guccini_
292. **Status symbol**   
		_L. Cerquetti – A. Mei – G. Carletti – M. Vecchi_
293. **Stella cieca**   
		_M. Vattai – M. Vecchi – G. Carletti_
294. **Stella d'oriente**   
		_G. Carletti – L. Cerquetti – A. Mei_
295. **Stop the world** (Vogliamo tornare a giocare)  
		_A. Mei – L. Cerquetti – G. Carletti – C. Falzone_
296. **Storie di mare**   
		_P. Milanesi_
297. **Stranamore** (Pure questo è amore)  
		_R. Vecchioni_
298. **Stringi i pugni**   
		_G. Carletti – D. Sacco – G. Salvatori_
299. **Suoni**   
		_G. Carletti – C. Contini – A. Daolio_

### T

300. **Tantum ergo**   
		_T. d'Aquino – Gregoriano – F. J. Haydn – G. Carletti_
301. **Tarassaco**   
		_G. Carletti – C. Turato – S. Reggioli – C. Turato_
302. **Te Deum**   
		_M. A. Charpentier – G. Carletti_
303. **Tempo che se ne va**   
		_M. Dapit – G. Carletti_
304. **Terra di nessuno**   
		_G. Carletti - M. Vecchi – A. Salerno_
305. **Terzo tempo**   
		_C. Cattini – E. Carletti – G. Carletti_
306. **Ti lascio una parola** (Goodbye)  
		_D. Pigozzo – R. Robin – G. Carletti_
307. **Ti porto a vivere**   
		_F. Ferrandi – G. Carletti – M. Petrucci – M. Rettani – M. Vecchi_
308. **Ti voglio** (I want you)  
		_G. Calabrese – B. Dylan_
309. **Toccami il cuore**   
		_G. Carletti – L. Cerquetti – L. Trentacarlini – M. Vecchi_
310. **Tornerò**   
		_G. P. Reverberi – A. Sacchi – S. Leva_
311. **Tra loro**   
		_M. Fumai_
312. **Trovare Dio**   
		_G. Carletti – D. Pignozzo – N. Pietrogrande_
313. **Tu che farai**   
		_A. Daolio – G. Carletti – C. Dennis_
314. **Tu combatterai**   
		_G. Carletti – M. Chiarelli_
315. **Tu puoi**   
		_G. Carletti – O. Veroli – Ripari – L. Cerquetti_
316. **Tutti quanti a credere**   
		_D. Campani – G. Carletti – C. Falzone – P. Milanesi – M. Vecchi_
317. **Tutto a posto**   
		_A. Salerno – B. Tavernese_
318. **Tutto passa**   
		_G. Carletti – C. Contini_
319. **Tutto vero**   
		_G. Carletti – M. Rovatti – M. vecchi_

### U

320. **Un'altra città**   
		_A. Salis – G. Carletti – O. Veroli_
321. **Un altro cielo**   
		_B. Rosset – M. Vecchi – L. Zannoni – G. Carletti_
322. **Una storia da raccontare**   
		_G. Carletti – P. Turtoro_
323. **Un attimo di sole**   
		_M. Vecchi – L. Cerquetti – L. Trentacarlini – G. Carletti_
324. **Un autunno insieme e poi**   
		_G. Carletti – C. Contini – D. Gilocchi_
325. **Un corpo e un'anima**   
		_G. P. Reverberi – G. Carletti_
326. **Un figlio dei fiori non pensa al domani** (Death of the Clown)  
		_D. Davies – F. Guccini_
327. **Un giorno insieme**   
		_L. Albertelli – Daiano – R. Soffici_
328. **Uno come noi**   
		_G. Cortesi_
329. **Uno sbaglio**   
		_C. Castellari – A. Salerno_
330. **Un po' di me**   
		_L. Albertelli – G. Carletti_
331. **Un pugno di sabbia**   
		_Daiano – R. Soffici_
332. **Un ricordo**   
		_G. Carletti – O. Veroli – R. Rossi_
333. **Un riparo per noi** (With a girl like you)  
		_R. Presley – Pontiack – T. Verona_
334. **Uomo di sole**   
		_O. Veroli_
335. **Utopia**   
		_G. Carletti – G. Pinori_

### V

336. **Va** (La mia vita va)  
		_G. Carletti - O. Veroli – D. Taurian_
337. **Vai via, cosa vuoi** (All the love in the world)  
		_G. Simpson – C. Contini_
338. **Vent'anni**   
		_E. Polito – G. Savio – G. Bigazzi_
339. **Vento caldo**   
		_G. Carletti_
340. **Vittima dei sogni**   
		_G. Carletti – C. Contini_
341. **Vivo forte**   
		_D. Taurian – G. Carletti – O. Veroli_
342. **Voci per cantare**   
		_A. Travaglini – G. Carletti – M. Vecchi – E. Pietrelli – Y. Cilloni_
343. **Voglia d'inverno**   
		_F. Ceccarelli – M. Dallari_
344. **Voglio ridere** (Gli occhi tuoi)  
		_P. Limiti – M. Migliardi_
345. **Vola**   
		_G. Carletti – C. Contini_
346. **Vola bambino** (Hi ho silver lining)  
		_Mogol – S. English – L. Weiss_
347. **Vorrei parlare**   
		_Gionata – R. Rossi_
348. **Vulcani**   
		_A. Mei – G. Carletti – L. Cerquetti_

### W

349. **What now my love**   
		_C. Sigman – P. Delanoë – G. Bécaud – M. Ravel_
