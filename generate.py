#!/usr/bin/env python3

## IMPORTS ##

from pathlib import Path
import re, os


## CONFIG ##

build_dir = Path('build')
working_dir = Path('.')
file_name = 'Nomadi'
canzoni_dir = Path('canzoni')
latex = [
  {'cmd':'latexmk', 'opt': ['-pdf', '"%source%"']},
  {'cmd':'latexmk', 'opt': ['-pdf', '-jobname="%source%_multicol"', '"%source%"'], 'pre':r"sed -i 's/%\\input{multicol\.tex}/\\input{..\/multicol\.tex}/g' '%source%.tex'"},
  {'cmd':'latexmk', 'opt': ['-pdf', '-jobname="%source%_landscape"', '"%source%"'], 'pre':r"sed -i 's/\\input{..\/multicol\.tex}/%\\input{multicol\.tex}/g;s/a4paper/a4paper,landscape/g;s/\\songcolumns{3}/\\songcolumns{4}/g' '%source%.tex'"},
  {'cmd':'latexmk', 'opt': ['-pdf', '-jobname="%source%_A5"', '"%source%"'], 'pre':r"sed -i 's/a4paper,landscape/a5paper/g;s/\\songcolumns{4}/\\songcolumns{2}/g' '%source%.tex'"},
  {'cmd':'python3 ../to_html/songs.py', 'opt': ['"%source%.tex"', '"%source%.html"', 'it', 'Indice'], 'post':'; '.join([f'ebook-convert "%source%.html" "%source%.{format}" --language italian --authors Nomadi {opt} --cover ../epub_cover.jpg --level1-toc //h:h2 --level2-toc //h:h3' for format, opt in (('epub', ''), ('mobi', ''))])}
]


## INIT ##

build_dir.mkdir(parents=True, exist_ok=True)


## GET SONGS AND INFO ##

songs = '' # LaTeX inputs of the songs
song_list = {} # list of songs info (title, optional subtitle and authors)

regexp = re.compile(r"\\beginsong\{([^\\\}]+)(?:\\tiny\\\\([^\}]+))?\}\[by=\{([^\}]*)\}\]") # To get song info

initial = '' # first letter of the last song
for s in sorted(canzoni_dir.glob('*.tex'), key=lambda s: str.casefold(str(s))):
  # For each song (in alphabetic order)
  with s.open() as file:
    fc = file.read().replace('~', ' ').replace('--', '–') # LaTeX to MD for info
    data = regexp.search(fc) # get info
    title = data.group(1)
    this_initial = s.name[0].upper()
    if this_initial.isnumeric():
      this_initial = '#'
    if initial != this_initial:
      # if the first letter changes
      initial = this_initial
      song_list[initial] = []
      songs += r'\begin{intersong}\hsection{' + ("\#" if initial == '#' else initial) + '}\\end{intersong}\n' # new section
    song_list[initial].append((
      title,
      data.group(2), # optional subtitle
      data.group(3)  # authors
    )) # add info to the list
  songs+=f'\\input{{../{s}}}\n' # input for the song

with (working_dir/'README.md').open() as file:
    # get info from README
    fc = file.read()
    ver = re.search(r'Versione *`([0-9\.]+)` *<br> *\n', fc).group(1) # version
    date = re.search(r'Ultimo aggiornamento: *([\w ]+) *\n', fc).group(1) # last update date


## GENERATE PDF ##

with (working_dir/f'{file_name}.tex').open() as file:
    # read LaTeX template
    tex = file.read()

# replace data into LaTeX template
tex = tex.replace(r'%%%SONGS%%%', songs) # song inputs
tex = tex.replace(r'<<<VERSION>>>', ver) # version
tex = tex.replace(r'<<<DATE>>>', date) # last update date

with (build_dir/f'{file_name}.tex').open('w') as file:
  # write LaTeX source
  file.write(tex)

for tool in latex:
  def exec_in_build_dir(cmd): os.system(f"cd {build_dir}; {cmd.replace('%source%', file_name)}")
  if 'pre' in tool:
    exec_in_build_dir(tool['pre'])
  exec_in_build_dir(f'{tool["cmd"]} {" ".join(tool["opt"])}') # generate PDF
  if 'post' in tool:
    exec_in_build_dir(tool['post'])


## GENERATE SONG LIST ##

with (working_dir/'canzoni.md').open('w') as file:
  file.write("# Nomadi\n## Il canzoniere\n[← indietro](README.md)\n## Indice alfabetico canzoni\n\n") # header
  i = 1
  for k in song_list:
    file.write(f"\n### {k}\n\n") # write heading for new section
    for s in song_list[k]:
      file.write(f"{i}. **{s[0]}** {f'({s[1]})' if s[1] else ''}  \n\t\t_{s[2]}_\n") # write song info
      i += 1

